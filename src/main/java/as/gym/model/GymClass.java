package as.gym.model;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="GymClass")
public class GymClass {
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private int id;
  
  @Column(nullable=false,columnDefinition="nvarchar(40)")
  private String name;
  
  @Column(nullable=false, name="date_happens")
  private LocalDateTime dateHappens;
  
  @Column(name="free_spots")
  private int freeSpots;
  
  @OneToOne(fetch=FetchType.LAZY)
  @JoinColumn(name="trainer_id", nullable=false)
  private User trainer;
  
  @ManyToMany(fetch=FetchType.LAZY, mappedBy="classes")
  private Set<User> participants = new HashSet<>();
  
  public GymClass(){}

  public GymClass(String name, LocalDateTime dateHappens, int freeSpots,
      User trainer) {
    super();
    this.name = name;
    this.dateHappens = dateHappens;
    this.freeSpots = freeSpots;
    this.trainer = trainer;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public LocalDateTime getDateHappens() {
    return dateHappens;
  }

  public void setDateHappens(LocalDateTime dateHappens) {
    this.dateHappens = dateHappens;
  }

  public int getFreeSpots() {
    return freeSpots;
  }

  public void setFreeSpots(int freeSpots) {
    this.freeSpots = freeSpots;
  }

  public User getTrainer() {
    return trainer;
  }

  public void setTrainer(User trainer) {
    this.trainer = trainer;
  }
  
  public Set<User> getParticipants() {
    return participants;
  }

  public void setParticipants(Set<User> participants) {
    this.participants = participants;
  }
  
  public void addParticipant(User u){
    this.participants.add(u);
  }

  public static GymClass of(GymClass gc){
    return new GymClass(gc.getName(),gc.getDateHappens(),gc.getFreeSpots(),gc.getTrainer());    
  }

  @Override
  public String toString() {
    return String.format(
        "GymClass [name=%s, dateHappens=%s, freeSpots=%s, trainer=%s]", name,
        dateHappens, freeSpots, trainer);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((dateHappens == null) ? 0 : dateHappens.hashCode());
    result = prime * result + freeSpots;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    GymClass other = (GymClass) obj;
    if (dateHappens == null) {
      if (other.dateHappens != null)
        return false;
    } else if (!dateHappens.equals(other.dateHappens))
      return false;
    if (freeSpots != other.freeSpots)
      return false;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    return true;
  }
  
  
  
}
