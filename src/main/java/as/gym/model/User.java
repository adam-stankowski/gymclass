package as.gym.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

@Entity
@Table(name="User")
public class User {
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private int id;
  
  @Size(min=3,max=8, message="Name has to be between 3 and 8 letters")
  @Column(nullable=false, columnDefinition="nvarchar(40)")
  @Type(type="nstring")
  private String name;
  
  @Size(min=3,max=20, message="Email has to be between 3 and 20 letters")
  @Column(nullable=false, columnDefinition="varchar(40)")
  private String email;
  
  @Size(min=3,max=20, message="Password has to be between 3 and 20 letters")
  @Column(nullable=false, columnDefinition="varchar(60)")
  private String password;
  
  @Column(nullable=false, columnDefinition="varchar(20)")
  private String role;
  
  @Column(nullable=false, columnDefinition="varchar(10)")
  @Enumerated(EnumType.STRING)
  private UserType type;
  
  @ManyToMany(fetch=FetchType.LAZY,cascade={CascadeType.ALL})
  @JoinTable(name="GymClass_User",
  joinColumns={@JoinColumn(name="user_id",nullable=false)},
  inverseJoinColumns={@JoinColumn(name="gym_class_id", nullable=false)})
  private Set<GymClass> classes = new HashSet<>();
  
  public User(){
  }
  
  
  public User(String name, String email, String password, String role,
      UserType type) {
    this.name = name;
    this.email = email;
    this.password = password;
    this.role = role;
    this.type = type;
  }


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public UserType getType() {
    return type;
  }

  public void setType(UserType type) {
    this.type = type;
  }
  
  public Set<GymClass> getClasses() {
    return classes;
  }

  public void setClasses(Set<GymClass> classes) {
    this.classes = classes;
  }
  
  public void addClass(GymClass c){
    this.classes.add(c);
  }


  public static User of(User u){
    return new User(u.getName(),u.getEmail(),u.getPassword(),u.getRole(),u.getType());
  }

  @Override
  public String toString() {
    return String.format("User [name=%s, email=%s, role=%s]", name, email, role);
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((email == null) ? 0 : email.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((role == null) ? 0 : role.hashCode());
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    return result;
  }


  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    User other = (User) obj;
    if (email == null) {
      if (other.email != null)
        return false;
    } else if (!email.equals(other.email))
      return false;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    if (role == null) {
      if (other.role != null)
        return false;
    } else if (!role.equals(other.role))
      return false;
    if (type != other.type)
      return false;
    return true;
  }
  
}
