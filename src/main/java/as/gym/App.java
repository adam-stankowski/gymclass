package as.gym;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import as.gym.model.User;

public class App {

  public static void main(String[] args) {
    try (
        FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext(
            "src/test/java/as/gym/test/config/test-context.xml")) {
      BasicDataSource ds = context.getBean(BasicDataSource.class, "dataSource");
      System.out.println(ds.getUrl());
      System.out.println(ds.getDriverClassName());
      System.out.println(ds.getUsername());
      System.out.println(ds.getPassword());
    }
  }

}
