package as.gym.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import as.gym.model.GymClass;
import as.gym.model.User;
import as.gym.service.BasicGymService;
import as.gym.service.BasicUserService;

@Controller
public class GymClassController {

  @Resource(name = "gymService")
  BasicGymService gymService;

  @Resource(name = "userService")
  BasicUserService userService;

  @RequestMapping("/join/{id}")
  @ResponseBody
  public Map<String, Object> getAllClasses(@PathVariable("id") Integer id,
      Principal principal) {
    String username = principal.getName();
    User u = userService.getByUsername(username).get();
    GymClass c = gymService.getSingle(id);
    u.getClasses().add(c);
    userService.update(u);

    Map<String, Object> result = new HashMap<>();
    result.put("status", "ok");
    List<Integer> ids = new ArrayList<>();
    ids.add(c.getId());
    result.put("gymClassIds", ids);

    return result;
  }

  @RequestMapping("/userClasses")
  @ResponseBody
  public Map<String, Object> getUsersClasses(Principal principal) {
    Map<String, Object> result = new HashMap<>();
    String username = principal.getName();

    result.put("status", "ok");
    result.put("gymClassIds", gymService.getClassesForUsername(username).stream()
        .map(GymClass::getId).collect(Collectors.toList()));

    return result;
  }

}
