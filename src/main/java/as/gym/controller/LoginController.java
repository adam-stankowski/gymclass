package as.gym.controller;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import as.gym.model.User;
import as.gym.model.UserType;
import as.gym.service.BasicUserService;

@Controller
public class LoginController {

  @Resource(name = "userService")
  private BasicUserService userService;

  @RequestMapping("/login")
  public String showLogin() {
    return "login";
  }

  @RequestMapping("/do_login")
  public String letIn() {
    return "/";
  }

  @RequestMapping(value = "/register", method = RequestMethod.GET)
  public String showRegistration(Model model) {
    User user = new User();
    model.addAttribute("user", user);
    return "register";
  }

  @RequestMapping(value = "/register", method = RequestMethod.POST)
  public ModelAndView registerUserAccount(@ModelAttribute("user") @Valid User user,
      BindingResult bindingResult, WebRequest webRequest, Errors errors) {
    
    if (bindingResult.hasErrors()) {
      return new ModelAndView("register", "user", user);
    }
    if (userService.getByUsername(user.getEmail()).isPresent()) {
      bindingResult.rejectValue("email",null, "This email already exists");
      return new ModelAndView("register", "user", user);
    } else {
      userService.registerUser(user);
    }

    return new ModelAndView("registrationSuccess", "user", user);
  }
}
