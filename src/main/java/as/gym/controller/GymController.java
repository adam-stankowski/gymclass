package as.gym.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import as.gym.model.GymClass;
import as.gym.model.User;
import as.gym.service.BasicGymService;
import as.gym.service.BasicUserService;

@Controller
public class GymController {
  
  @Autowired
  BasicGymService gymService;
  
  @Autowired
  BasicUserService userService;
  
  @RequestMapping("/")
  public String showHome(Model model, Principal principal){
    
    model.addAttribute("gymClasses", gymService.getAllClasses());
    return "home";
  }

}
