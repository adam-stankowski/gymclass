package as.gym.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

@Transactional
public abstract class AbstractJpaDao <T> {
  
  private Class<T> clazz;
  
  @PersistenceContext
  EntityManager em;
  
  public final void setClass(Class<T> clazzToSet){
    this.clazz = clazzToSet;
  }
  public void insert(T item){
    em.persist(item);    
  }
  public void update(T item){
    em.merge(item);
  }
  public void delete(T item){
  //remove(item) itself throws exception of removing a detached object
    em.remove(em.contains(item) ? item : em.merge(item));     
  }
  public List<T> getAll(){
    return em.createQuery(String.format("select x from %s x", clazz.getName()), clazz).getResultList();
  }
  public T getSingle(int id){
    return em.find(clazz, id);
  }
}
