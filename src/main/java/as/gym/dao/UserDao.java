package as.gym.dao;

import java.util.Optional;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import as.gym.model.User;

@Qualifier("userDao")
@Component
@Transactional
public class UserDao extends AbstractJpaDao<User> implements IUserDao {

  public UserDao() {
    super();
    setClass(User.class);
  }

  @Override
  public Optional<User> getByEmail(String email) {
    try{
      User u = em.createQuery(
          "from User user left join fetch user.classes where user.email=:email",
          User.class).setParameter("email", email).getSingleResult();
      return Optional.of(u);
    }catch(NoResultException e){
      return Optional.empty();
    }
  }
}
