package as.gym.dao;

import java.util.List;

public interface Dao<T> {
  public void insert(T item);
  public void update(T item);
  public void delete(T item);
  public List<T> getAll();
  public T getSingle(int id);
}
