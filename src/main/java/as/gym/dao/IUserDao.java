package as.gym.dao;

import java.util.Optional;

import as.gym.model.User;

public interface IUserDao extends Dao<User>{
  Optional<User> getByEmail(String email);
}
