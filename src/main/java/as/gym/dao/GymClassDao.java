package as.gym.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import as.gym.model.GymClass;

@Qualifier("gymClassDao")
@Component
@Transactional
public class GymClassDao extends AbstractJpaDao<GymClass> implements IGymClassDao {
  public GymClassDao() {
    super();
    this.setClass(GymClass.class);
  }

  @Override
  public List<GymClass> getClassesForEmail(String email) {
    return em.createQuery(
        "from GymClass gymclass join fetch gymclass.participants where email=:email",
        GymClass.class).setParameter("email", email).getResultList();
  }
}
