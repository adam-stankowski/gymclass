package as.gym.dao;

import java.util.List;

import as.gym.model.GymClass;

public interface IGymClassDao extends Dao<GymClass>{

  List<GymClass> getClassesForEmail(String name);

}
