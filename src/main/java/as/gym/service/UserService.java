package as.gym.service;

import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import as.gym.dao.IUserDao;
import as.gym.model.User;
import as.gym.model.UserType;

@Service("userService")
public class UserService implements BasicUserService{
  
  @Resource(name="userDao")
  IUserDao userDao;

  @Override
  public Optional<User> getByUsername(String username) {
    return userDao.getByEmail(username);
  }

  @Override
  public void insert(User u) {
    userDao.insert(u);
  }

  @Override
  public void update(User u) {
    userDao.update(u);    
  }

  @Override
  public void registerUser(User user) {
    user.setRole("ROLE_USER");
    user.setType(UserType.USER);
    insert(user);
  }
  

}
