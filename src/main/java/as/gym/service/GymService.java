package as.gym.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import as.gym.dao.Dao;
import as.gym.dao.IGymClassDao;
import as.gym.model.GymClass;

@Service("gymService")
public class GymService implements BasicGymService{
  
  @Resource(name="gymClassDao")
  private IGymClassDao gymClassDao;
  
  public List<GymClass> getAllClasses(){
    return gymClassDao.getAll();
  }
  
  @Override
  public GymClass getSingle(int id){
    return gymClassDao.getSingle(id);
  }

  @Override
  public List<GymClass> getClassesForUsername(String name) {
    return gymClassDao.getClassesForEmail(name);
  }

}
