package as.gym.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import as.gym.model.User;

@Service
public interface BasicUserService {
  Optional<User> getByUsername(String username);

  void insert(User user);

  void update(User user);
  
  void registerUser(User user);
}
