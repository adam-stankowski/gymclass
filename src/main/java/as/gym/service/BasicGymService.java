package as.gym.service;

import java.util.List;

import as.gym.model.GymClass;

public interface BasicGymService {
  
  List<GymClass> getAllClasses();

  GymClass getSingle(int gymClassId);

  List<GymClass> getClassesForUsername(String name);

}
