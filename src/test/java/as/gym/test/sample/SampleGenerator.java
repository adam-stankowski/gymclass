package as.gym.test.sample;

import java.util.List;

public interface SampleGenerator<T> {
  T[] getFullSample(int howMany);
  T getSingle(int index);
  T getClonedSingle(int index);
  T getRandomSingle();
}
