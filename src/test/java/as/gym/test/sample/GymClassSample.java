package as.gym.test.sample;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;

import as.gym.model.GymClass;

public class GymClassSample implements SampleGenerator<GymClass> {
  
  private final GymClass[] gymClassSamples;
  
  public GymClassSample(UserSample userSample){
    
    gymClassSamples = new GymClass[] {
        new GymClass("WOD", LocalDateTime.of(2017, 1, 12, 18, 0), 12, userSample.getSingle(0)),
        new GymClass("WOD", LocalDateTime.of(2017, 1, 12, 19, 0), 12, userSample.getSingle(1)),
        new GymClass("WOD", LocalDateTime.of(2017, 1, 12, 20, 0), 12,
            userSample.getSingle(2)) };
  }

  @Override
  public GymClass[] getFullSample(int howMany) {
    if(howMany < 0 || howMany > gymClassSamples.length){
      throw new IllegalArgumentException("The number of test users cannot be more than 5");
    }
    return Arrays.copyOf(gymClassSamples, howMany);  
  }

  @Override
  public GymClass getClonedSingle(int index) {
    if(index < 0 || index > gymClassSamples.length){
      throw new IllegalArgumentException("The number of test users cannot be more than 5");
    }
    return GymClass.of(gymClassSamples[index]);
  }

  @Override
  public GymClass getRandomSingle() {
    Random r = new Random();
    return gymClassSamples[r.nextInt(gymClassSamples.length)];
  }

  @Override
  public GymClass getSingle(int index) {
    if(index < 0 || index > gymClassSamples.length){
      throw new IllegalArgumentException("The number of test users cannot be more than 5");
    }
    return gymClassSamples[index];
  }

}
