package as.gym.test.sample;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import as.gym.model.User;
import as.gym.model.UserType;

public class UserSample implements SampleGenerator<User>{
  
  private final User[] userSamples;
  
  public UserSample(){
    
    userSamples = new User[] {
        new User("Adam Jones", "adam@wp.pl", "password", "ROLE_USER", UserType.USER),
        new User("John Blake", "john@wp.pl", "password", "ROLE_USER", UserType.COACH),
        new User("Freddy Jr", "freddy@wp.pl", "password", "ROLE_USER", UserType.USER),
        new User("Mick Jagger", "Mick@wp.pl", "password", "ROLE_ADMIN", UserType.COACH),
        new User("John Scott", "john_scott@wp.pl", "password", "ROLE_USER", UserType.USER),
    };
    
  }

  @Override
  public User[] getFullSample(int howMany) {
    if(howMany < 0 || howMany > userSamples.length){
      throw new IllegalArgumentException("The number of test users cannot be more than 5");
    }
    return Arrays.copyOf(userSamples, howMany);    
  }

  @Override
  public User getClonedSingle(int index) {
    if(index < 0 || index > userSamples.length){
      throw new IllegalArgumentException("The number of test users cannot be more than 5");
    }
    return User.of(userSamples[index]);
  }

  @Override
  public User getRandomSingle() {
    Random r = new Random();
    return userSamples[r.nextInt(userSamples.length)];
  }

  @Override
  public User getSingle(int index) {
    if(index < 0 || index > userSamples.length){
      throw new IllegalArgumentException("The number of test users cannot be more than 5");
    }
    return userSamples[index];
  }

}
