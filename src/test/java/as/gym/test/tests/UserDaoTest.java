package as.gym.test.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Optional;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import as.gym.dao.Dao;
import as.gym.dao.IUserDao;
import as.gym.model.GymClass;
import as.gym.model.User;
import as.gym.test.sample.GymClassSample;
import as.gym.test.sample.UserSample;

@ContextConfiguration(locations = { "classpath:as/gym/test/config/test-context.xml",
    "classpath:as/gym/config/db-context.xml",
    "classpath:as/gym/config/dao-context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class UserDaoTest {

  @Autowired
  @Qualifier("userDao")
  private IUserDao userDao;

  @Autowired
  @Qualifier("gymClassDao")
  private Dao<GymClass> gymClassDao;

  @Autowired
  private DataSource dataSource;

  private JdbcTemplate jdbc;

  private UserSample userSample;
  private GymClassSample gymClassSample;

  public UserDaoTest() {
    userSample = new UserSample();
    gymClassSample = new GymClassSample(userSample);
  }

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  @Before
  public void cleanDatabase() {
    if (jdbc == null) {
      jdbc = new JdbcTemplate(dataSource);
    }
    jdbc.execute("delete from GymClass_User");
    jdbc.execute("delete from GymClass");
    jdbc.execute("delete from User");

  }

  @Test
  public void given_iInsertOneUser_when_iCountUsersInDatabase_then_iShouldGetOne() {
    userDao.insert(userSample.getSingle(0));

    // Firing direct query to avoid a situation when an entity is cached and not
    // persisted in table
    int numRows = jdbc.queryForObject("select count(*) from User", Integer.class);
    assertEquals(1, numRows);
  }

  @Test
  public void given_iInsertTwoUsers_when_iCountUsersInDatabase_then_iShouldGetTwo() {
    userDao.insert(userSample.getSingle(0));
    userDao.insert(userSample.getSingle(1));

    // Firing direct query to avoid a situation when an entity is cached and not
    // persisted in table
    int numRows = jdbc.queryForObject("select count(*) from User", Integer.class);
    assertEquals(2, numRows);
  }

  @Test
  public void given_aUsrInAdatabase_when_modifiedStoredAndRetrieved_then_iShouldHaveTheSameUser() {
    User u = User.of(userSample.getSingle(0));
    userDao.insert(u);
    u.setEmail("modifiedemail@com.pl");
    userDao.update(u);
    User u2 = userDao.getSingle(u.getId());
    assertEquals(u, u2);
  }

  @Test
  public void given_iSaveManyUsersInDB_when_iRetrieveThem_iShouldRetrieveTheRightAmount() {

    User[] testUsers = userSample.getFullSample(5);

    String sql = String.format(
        "insert into User (name,email,password,role,type) values ('%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s')",
        testUsers[0].getName(), testUsers[0].getEmail(), testUsers[0].getPassword(),
        testUsers[0].getRole(), testUsers[0].getType(), testUsers[1].getName(),
        testUsers[1].getEmail(), testUsers[1].getPassword(), testUsers[1].getRole(),
        testUsers[1].getType(), testUsers[2].getName(), testUsers[2].getEmail(),
        testUsers[2].getPassword(), testUsers[2].getRole(), testUsers[2].getType());
    jdbc.update(sql);
    assertEquals(3, userDao.getAll().size());
  }

  @Test
  public void given_iHaveFewRowsInTable_when_iDeleteOneRow_iShouldHaveOneRowLessInDB() {

    User[] testUsers = userSample.getFullSample(5);

    String sql = String.format(
        "insert into User (name,email,password,role,type) values ('%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s')",
        testUsers[0].getName(), testUsers[0].getEmail(), testUsers[0].getPassword(),
        testUsers[0].getRole(), testUsers[0].getType(), testUsers[1].getName(),
        testUsers[1].getEmail(), testUsers[1].getPassword(), testUsers[1].getRole(),
        testUsers[1].getType(), testUsers[2].getName(), testUsers[2].getEmail(),
        testUsers[2].getPassword(), testUsers[2].getRole(), testUsers[2].getType());
    User u = User.of(testUsers[3]);

    userDao.insert(u);
    jdbc.update(sql);
    assertEquals(4, userDao.getAll().size());
    userDao.delete(u);

    assertEquals(3, userDao.getAll().size());
  }

  @Test
  public void given_userAndGymClass_when_iSignUserUp_then_iRetrieveUserIsSignedToClass() {
    User u = userSample.getSingle(0);
    User coach = userSample.getSingle(1);
    userDao.insert(u);
    userDao.insert(coach);

    GymClass c = gymClassSample.getSingle(1);
    c.setTrainer(coach);
    gymClassDao.insert(c);

    u.addClass(c);

    userDao.update(u);

    int numRows = jdbc.queryForObject("select count(*) from GymClass_User",
        Integer.class);
    assertEquals(1, numRows);
  }

  @Test
  public void given_userAndGymClass_when_iSignUpSeveralUsers_then_iRetrieveAdequateAmountOfRecords() {
    User u = userSample.getSingle(0);
    User u2 = userSample.getSingle(2);
    User u3 = userSample.getSingle(3);
    User coach = userSample.getSingle(1);
    userDao.insert(u);
    userDao.insert(u2);
    userDao.insert(u3);
    userDao.insert(coach);

    GymClass c = gymClassSample.getSingle(1);
    c.setTrainer(coach);
    gymClassDao.insert(c);

    u.addClass(c);
    u2.addClass(c);
    u3.addClass(c);

    userDao.update(u);
    userDao.update(u2);
    userDao.update(u3);

    int numRows = jdbc.queryForObject("select count(*) from GymClass_User",
        Integer.class);
    assertEquals(3, numRows);
  }

  @Test
  public void given_userIsAssignedToAClass_when_iRemoveTheAssignment_then_assignmentIsRemovedFromDB() {
    User u = userSample.getSingle(0);
    User coach = userSample.getSingle(1);
    userDao.insert(u);
    userDao.insert(coach);

    GymClass c = gymClassSample.getSingle(1);
    c.setTrainer(coach);
    gymClassDao.insert(c);

    u.addClass(c);

    assertEquals(1, u.getClasses().size());

    u.getClasses().remove(c);
    userDao.update(u);

    int numRows = jdbc.queryForObject("select count(*) from GymClass_User",
        Integer.class);
    assertEquals(0, numRows);
  }

  @Test
  public void given_userWithParticularNameExists_when_iRetrieveByUsername_then_iShouldGetThatUser() {
    User[] testUsers = userSample.getFullSample(5);

    String sql = String.format(
        "insert into User (name,email,password,role,type) values ('%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s')",
        testUsers[0].getName(), testUsers[0].getEmail(), testUsers[0].getPassword(),
        testUsers[0].getRole(), testUsers[0].getType(), testUsers[1].getName(),
        testUsers[1].getEmail(), testUsers[1].getPassword(), testUsers[1].getRole(),
        testUsers[1].getType(), testUsers[2].getName(), testUsers[2].getEmail(),
        testUsers[2].getPassword(), testUsers[2].getRole(), testUsers[2].getType());
    
    //Avoiding EntityManager caching
    jdbc.update(sql);
    
    User u = userDao.getByEmail(testUsers[1].getEmail()).get();
    assertNotNull(u);
    assertEquals(testUsers[1].getName(), u.getName());

  }
  
  @Test
  public void given_userWithParticularNameDoesNotExist_when_iRetrieveByUsername_then_throwException() {
    User[] testUsers = userSample.getFullSample(5);

    String sql = String.format(
        "insert into User (name,email,password,role,type) values ('%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s'),('%s','%s','%s','%s','%s')",
        testUsers[0].getName(), testUsers[0].getEmail(), testUsers[0].getPassword(),
        testUsers[0].getRole(), testUsers[0].getType(), testUsers[1].getName(),
        testUsers[1].getEmail(), testUsers[1].getPassword(), testUsers[1].getRole(),
        testUsers[1].getType(), testUsers[2].getName(), testUsers[2].getEmail(),
        testUsers[2].getPassword(), testUsers[2].getRole(), testUsers[2].getType());
    
    //Avoiding EntityManager caching
    jdbc.update(sql);
    
    Optional<User> u = userDao.getByEmail("wrongemail");
    assertFalse(u.isPresent());

  }

}
