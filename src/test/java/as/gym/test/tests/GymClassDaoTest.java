package as.gym.test.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.time.LocalDateTime;
import java.util.Arrays;

import javax.sql.DataSource;

import org.hibernate.engine.jdbc.connections.internal.UserSuppliedConnectionProviderImpl;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import as.gym.dao.Dao;
import as.gym.dao.IGymClassDao;
import as.gym.dao.IUserDao;
import as.gym.model.GymClass;
import as.gym.model.User;
import as.gym.model.UserType;
import as.gym.test.sample.GymClassSample;
import as.gym.test.sample.UserSample;

@ContextConfiguration(locations = { "classpath:as/gym/test/config/test-context.xml",
    "classpath:as/gym/config/db-context.xml", "classpath:as/gym/config/dao-context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class GymClassDaoTest {

  @Autowired
  @Qualifier("gymClassDao")
  private IGymClassDao gymClassDao;

  @Autowired
  @Qualifier("userDao")
  private IUserDao userDao;

  @Autowired
  private DataSource dataSource;

  private JdbcTemplate jdbc;

  private UserSample userSample;
  private GymClassSample gymClassSample;

  public GymClassDaoTest() {
    this.userSample = new UserSample();
    this.gymClassSample = new GymClassSample(userSample);
  }

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  @Before
  public void cleanDatabase() {
    if (jdbc == null) {
      jdbc = new JdbcTemplate(dataSource);
    }
    jdbc.execute("delete from GymClass_User");
    jdbc.execute("delete from GymClass");
    jdbc.execute("delete from User");

  }

  @Test
  public void given_iInsertOneClass_when_iCountClassesInDatabase_then_iShouldGetOne() {
    userDao.insert(userSample.getSingle(0));
    gymClassDao.insert(gymClassSample.getSingle(0));

    // Firing direct query to avoid a situation when an entity is cached and not
    // persisted in table
    int numRows = jdbc.queryForObject("select count(*) from GymClass",
        Integer.class);
    assertEquals(1, numRows);
  }

  @Test
  public void given_iInsertThreeClasses_when_iCountClassesInDatabase_then_iShouldGetThree() {
    userDao.insert(userSample.getSingle(0));
    userDao.insert(userSample.getSingle(1));
    userDao.insert(userSample.getSingle(2));
    gymClassDao.insert(gymClassSample.getSingle(0));
    gymClassDao.insert(gymClassSample.getSingle(1));
    gymClassDao.insert(gymClassSample.getSingle(2));

    // Firing direct query to avoid a situation when an entity is cached and not
    // persisted in table
    int numRows = jdbc.queryForObject("select count(*) from GymClass",
        Integer.class);
    assertEquals(3, numRows);
  }

  @Test
  public void given_aClassInAdatabase_when_modifiedStoredAndRetrieved_then_iShouldHaveTheSameClass() {
    User u = User.of(userSample.getSingle(0));
    GymClass g = GymClass.of(gymClassSample.getSingle(0));
    g.setTrainer(u);
    userDao.insert(u);
    gymClassDao.insert(g);
    g.setName("Modified class");
    gymClassDao.update(g);
    GymClass g2 = gymClassDao.getSingle(g.getId());
    assertEquals(g, g2);
  }

  @Test
  public void given_iSaveManyClassesInDB_when_iRetrieveThem_iShouldRetrieveTheRightAmount() {

    Arrays.stream(userSample.getFullSample(5)).forEach(u -> userDao.insert(u));

    String sql = String.format(
        "insert into GymClass (name,date_happens,free_spots,trainer_id) values ('%s','%s',%d,%d),('%s','%s',%d,%d),('%s','%s',%d,%d)",
        gymClassSample.getSingle(0).getName(), gymClassSample.getSingle(0).getDateHappens().toString(),
        gymClassSample.getSingle(0).getFreeSpots(), gymClassSample.getSingle(0).getTrainer().getId(),
        gymClassSample.getSingle(1).getName(), gymClassSample.getSingle(1).getDateHappens().toString(),
        gymClassSample.getSingle(1).getFreeSpots(), gymClassSample.getSingle(1).getTrainer().getId(),
        gymClassSample.getSingle(2).getName(), gymClassSample.getSingle(2).getDateHappens().toString(),
        gymClassSample.getSingle(2).getFreeSpots(), gymClassSample.getSingle(2).getTrainer().getId());
    jdbc.update(sql);

    assertEquals(3, gymClassDao.getAll().size());
  }

  @Test
  public void given_iHaveFewClassesInTable_when_iDeleteOneRow_iShouldHaveOneRowLessInDB() {
    Arrays.stream(userSample.getFullSample(5)).forEach(u -> userDao.insert(u));

    String sql = String.format(
        "insert into GymClass (name,date_happens,free_spots,trainer_id) values ('%s','%s',%d,%d),('%s','%s',%d,%d),('%s','%s',%d,%d)",
        gymClassSample.getSingle(0).getName(), gymClassSample.getSingle(0).getDateHappens().toString(),
        gymClassSample.getSingle(0).getFreeSpots(), gymClassSample.getSingle(0).getTrainer().getId(),
        gymClassSample.getSingle(1).getName(), gymClassSample.getSingle(1).getDateHappens().toString(),
        gymClassSample.getSingle(1).getFreeSpots(), gymClassSample.getSingle(1).getTrainer().getId(),
        gymClassSample.getSingle(2).getName(), gymClassSample.getSingle(2).getDateHappens().toString(),
        gymClassSample.getSingle(2).getFreeSpots(), gymClassSample.getSingle(2).getTrainer().getId());

    gymClassDao.insert(gymClassSample.getSingle(0));
    jdbc.update(sql);
    assertEquals(4, gymClassDao.getAll().size());
    gymClassDao.delete(gymClassSample.getSingle(0));

    assertEquals(3, gymClassDao.getAll().size());
  }
  
  @Test
  public void given_iDoNotHaveAnyClasses_when_iRequestClassesForUsername_iShouldGetEmptyList(){
    assertEquals(0, gymClassDao.getClassesForEmail("testName").size());
  }
  
  @Test
  public void given_iHaveClasses_when_iRequestClassesForUsername_iShouldGetMyClasses(){
    
    Arrays.stream(userSample.getFullSample(5)).forEach(u -> userDao.insert(u));
    
    Arrays.stream(gymClassSample.getFullSample(3)).forEach(g -> gymClassDao.insert(g));
    
    String sql = String.format(
        "insert into GymClass_User (gym_class_id,user_id) values (%d,%d),(%d,%d),(%d,%d)",
        gymClassSample.getSingle(0).getId(),userSample.getSingle(0).getId(),
        gymClassSample.getSingle(1).getId(),userSample.getSingle(0).getId(),
        gymClassSample.getSingle(0).getId(),userSample.getSingle(1).getId());
    jdbc.update(sql);
    assertEquals(2, gymClassDao.getClassesForEmail(userSample.getSingle(0).getEmail()).size());        
    
  }

}
