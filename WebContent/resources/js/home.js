/**
 * 
 */

var gymClassHandlers = {
	joinSuccess : function(data, textStatus){
		let ids = data.gymClassIds;
		for(let i = 0; i < ids.length; i++){
			$('#joinLink'+ids[i]).text('Quit');
		}
		return false;
	}	
};

var gymClassFetchers = {
	join : function (event, id){		
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		$.ajax({
			url: '/GymClass/join/'+id,
			type:"POST",
			dataType:"json",
			data : {_csrf : token}
		}).done(gymClassHandlers.joinSuccess);
	}, 
	getClassesForUser : function(){
		var token = $("meta[name='_csrf']").attr("content");
		$.ajax({
			url: '/GymClass/userClasses',
			type:"GET",
			dataType:"json"
		}).done(gymClassHandlers.joinSuccess);
	}
}

$(document).ready(gymClassFetchers.getClassesForUser());