<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href='<c:url value="/res/css/style.css"></c:url>' />
<title>Insert title here</title>
</head>
<body>
	<div id="container">
		<div id="head"><h1>Login to Gym Class</h1></div>
		<div id="content">
			<c:if test="${param.error!=null}">
				<p class="error">There was an issue with your login. Please try
					again <br/>
				<c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />
				</p>
				
		</c:if>

			<form name='f' action='<c:url value="/do_login"></c:url>'
				method='POST'>
				<table>
					<tr>
						<td>Email:</td>
						<td><input type='text' name='username' value=''></td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type='password' name='password' /></td>
					</tr>
					<tr>
						<td><input name="submit" type="submit" value="submit" /></td>
					</tr>
				</table>
				<input name="${_csrf.parameterName}" type="hidden"
					value="${_csrf.token}" />
			</form>
		</div>
		<div id="register">
		
		<a href="<c:url value='/register'></c:url>">Register here!</a>
		</div>

	</div>

</body>
</html>
<script>
	document.f.username.focus();
</script>