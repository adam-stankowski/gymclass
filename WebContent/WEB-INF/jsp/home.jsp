<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href='<c:url value="/res/css/style.css"></c:url>' />
<script src="https://code.jquery.com/jquery-3.1.1.min.js"
	integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
	crossorigin="anonymous"></script>
<script type="text/javascript"
	src='<c:url value="/res/js/home.js"></c:url>'></script>
<meta name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}" />
<title>Gym Class Home</title>
</head>
<body>

	<div id="container">
		<div id="head">
			<h1>Welcome to Gym Class</h1>
			<sec:authorize access="isAuthenticated()">
				<div class="loginControl">
					Hello
					<sec:authentication property="principal.username" />
				</div>
			</sec:authorize>
			<sec:authorize access="isAnonymous()">
				<a class="login" href='<c:url value="/login"></c:url>'>Login</a>
			</sec:authorize>
		</div>
		<div id="content">

			<table id="gymClasses">
				<tr>
					<sec:authorize access="isAuthenticated()">
						<th>Action</th>
					</sec:authorize>
					<th>Class</th>
					<th>Time</th>
					<th>Free Spots</th>
				</tr>
				<c:forEach var="gymClass" items="${gymClasses}">
					<tr>
						<sec:authorize access="isAuthenticated()">
							<td><a id="joinLink${gymClass.id}"
								onclick="gymClassFetchers.join(event,${gymClass.id})" href="#">Join</a>
							</td>
						</sec:authorize>
						<td>${gymClass.name}</td>
						<td>${gymClass.dateHappens}</td>
						<td>${gymClass.freeSpots}</td>
					</tr>
				</c:forEach>
			</table>

		</div>
		<div id="footer">

			<a href="<c:url value="/admin"></c:url>">Admin</a>
		</div>
	</div>
</body>
</html>