<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<c:url value='/res/css/style.css'></c:url>"/>
<title>Register at Gym Class</title>
</head>
<body>

	<h1>Register at Gym Class</h1>
	<form:form modelAttribute="user" method="POST" enctype="utf8">
		<br>
		<table>
			<tr>
				<td><label> Username </label></td>
				<td><form:input path="name" value="" /></td>
				<form:errors path="name" element="div" cssClass="formError"/>
			</tr>
			<tr>
				<td><label> Email </label></td>
				<td><form:input path="email" value="" /></td>
				<form:errors path="email" element="div" cssClass="formError" />
			</tr>
			<tr>
				<td><label> Password </label></td>
				<td><form:input path="password" value="" type="password" /></td>
				<form:errors path="password" element="div" cssClass="formError" />
			</tr>
			<tr>
				<td><label> Confirm Password </label></td>
				<td><input value="" type="password" /></td>
				<form:errors element="div" />
			</tr>
			<tr>
				<td colspan="2">
					<button type="submit">Submit</button>
				</td>
			</tr>
		</table>
	</form:form>

</body>
</html>