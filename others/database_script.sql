CREATE DATABASE GymClass_test;
use GymClass_test;

CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) CHARACTER SET utf8 NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(60) NOT NULL,
  `role` varchar(20) NOT NULL,
  `type` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `GymClass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) CHARACTER SET utf8 NOT NULL,
  `date_happens` datetime NOT NULL,
  `free_spots` int(11) NOT NULL DEFAULT '12',
  `trainer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trainer_id` (`trainer_id`),
  CONSTRAINT `GymClass_ibfk_1` FOREIGN KEY (`trainer_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

create user gymclass_test@localhost identified by 'gymclass_test';
grant select,insert,update,delete on GymClass_test.* to gymclass_test;


alter table User add column enabled BIT not null default 1;
select * from User;

CREATE TABLE `GymClass_User` (
  `gym_class_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  UNIQUE KEY `gym_class_id` (`gym_class_id`,`user_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `GymClass_User_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`),
  CONSTRAINT `GymClass_User_ibfk_1` FOREIGN KEY (`gym_class_id`) REFERENCES `GymClass` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

select * from GymClass_User;


